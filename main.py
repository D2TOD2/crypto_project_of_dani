




from crypto.app import App
from crypto.market_events.event import MockEvent
from crypto.message_delivery.sender import MockSender
# We have not implemented any senders yet, we use a mock
# sender to test that the objects are well connected
senders = [MockSender()]
# We also use a mock event for the same purpose
events = [MockEvent(senders)]
# An app instance is created and run. The mock event is
# the only event connected to it so far.
app = App(events, 5)
app.start()
